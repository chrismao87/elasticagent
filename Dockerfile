FROM amd64/ubuntu

WORKDIR /opt/elastic/agent

ADD https://artifacts.elastic.co/downloads/beats/elastic-agent/elastic-agent-7.12.1-linux-x86_64.tar.gz .

RUN tar xzvf elastic-agent-7.12.1-linux-x86_64.tar.gz
RUN cp -R elastic-agent-7.12.1-linux-x86_64/* ./

CMD ./elastic-agent enroll -f -i --enrollment-token=RTNWT3BIa0JFSUVQNDVQMDJSNC06YmtGMWZFdVZTb081dlhWaHBGb253Zw== --kibana-url=https://9159ac25aba8454fa17a496049e143f2.us-west-1.aws.found.io:443 && ./elastic-agent run
